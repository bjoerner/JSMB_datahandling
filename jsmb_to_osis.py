import requests
import pandas as pd
from influxdb_client import InfluxDBClient


def read_influx_db(start_time, stop_time):      # query data of the timerange
    my_org = 'IOW'
    my_token = 'uXAP7W64LoC7KNVhcDjQzB97Gcw-BaZuOnZn7s0UxbAdFuSIysEnbfMWN32CnWG8pbFzzMrMeZ-DNT51IRyGQw=='
    client = InfluxDBClient(url="https://mqtt-influx.ostseeforschung.info", token=my_token, org=my_org)
    query_api = client.query_api()

    # prepare query for data in between start and stop date
    query = 'from(bucket: "JSMB")\
            |> range(start:' + start_time + ', stop: ' + stop_time + ')\
            |> filter(fn: (r) => r["_measurement"] == "EMB")\
            |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")'

    data_frame = query_api.query_data_frame(query)

    return data_frame


columns_to_drop = ['result', '_start', '_stop', 'table', '_measurement', 'stream', 'sensorId', 'flow', 'flow1', 'flow2']    # these columns are not needed
df_data = read_influx_db(str((pd.Timestamp.utcnow() - pd.Timedelta(minutes=10)).isoformat())[:-6] + 'Z', str(pd.Timestamp.utcnow().isoformat())[:-6] + 'Z')
# df_data = read_influx_db('2024-04-12T00:00:00.000Z', '2024-04-24T00:00:00.000Z')
if df_data.empty:
    print('no data')
else:
    df_data = df_data.drop(columns=columns_to_drop)
    # paraemters for OSIS DB, see documentation
    context_name = 'Longterm_Observation_IOW'
    datastream = "Import_EMB"
    data_format = "emb_jsmb_v1_m3"
    platform_shortname = "RV_Elisabeth_Mann_Borgese"

    # reformat timestamps for OSIS
    times = []
    for index, row in df_data.iterrows():
        times.append((row._time.isoformat()) + "Z")

    # prepare dictionary with data of dataframe
    d = {}
    df_data['timestamp'] = times
    df_data = df_data.drop(columns=['_time'])
    d['timestamp'] = list(df_data.timestamp)
    d['chlorophyllA'] = list(df_data.chlorophyllA)
    d['latitude'] = list(df_data.latitude)
    d['longitude'] = list(df_data.longitude)
    d['conductivity'] = list(df_data.tsg_conductivity)
    d['salinity'] = list(df_data.tsg_salinity)
    d['temperature'] = list(df_data.sea_water_temperature)
    d['turbidity'] = list(df_data.turbidity)
    d['heading'] = list(df_data.heading)
    d['speed'] = list(df_data.speed)
    d['course_over_ground'] = list(df_data.courseOverGround)
    df_osis = pd.DataFrame(data=d)
    print(f'The data has {len(df_osis)} positions')

    # prepare all positions in a loop
    payloads = df_osis.to_dict(orient='records')
    positions = [
        {
                    "contact_person": "regine.labrenz@io-warnemuende.de",
                    "datastream": datastream,
                    "latitude": payloads[i]['latitude'],
                    "longitude": payloads[i]['longitude'],
                    "obs_timestamp": payloads[i]['timestamp'][0:-7] + '.000Z',
                    "payload": {"data": payloads[i], "data_format": data_format},
                    "platform": platform_shortname
        }
        for i in range(len(df_osis))]
    print(positions[-1])

    # insert into database
    api_url = "http://10.10.10.124:5000/api/v1/"


    assert datastream in [pf['shortname'] for pf in requests.get(api_url + 'datastreams').json()]

    # check if this platform is already registered
    assert platform_shortname in [pf['shortname'] for pf in requests.get(api_url + 'platforms').json()]

    # check if platform is in context
    assert platform_shortname in [pf['shortname'] for pf in requests.get(api_url + 'contexts/' + context_name + '/platforms').json()]

    # insert the data as positions
    for p in positions:
        r = requests.post(api_url + 'positions', json=p)
        assert r.ok
