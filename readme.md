# Data Handling of the JSMB

This repository contains files to handle the data of the JSMB on land this includes

1. Grafana visualisation
	
	GrafanaVisualisation.json	-	the JSON can be imported into a new dashboard, and contains all the plots. Make sure to have the source configured properly in Grafana.
	
	The visualisation runs in https://mqtt.ostseeforschung.info/grafana/d/9ehaYLA4k/jsmb2?orgId=1&refresh=1m and can be accessed public or with your IOW login. For editing rights, ask Steffen Bock.

2. NodeRed

	nodeRed_EMB.json	-	this JSON inlcudes the flow for NodeRed to gather the data of the JSMB and push them to the InfluxDB.

3. Export to OSIS DB

	The python jsmb_to_osis.py adds data from the InfluxDB of IOW to the OSIS DB of IOW. Therefore it needs the libraries: requests, pandas, influxdb_client.
	
	The script is executed every 10 minutes by a cron job on a server by Steffen Bock.